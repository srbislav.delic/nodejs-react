"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
const logger = require("morgan");
var helmet = require("helmet");
var cors = require("cors");
var v1_1 = require("./router/v1");
var main_1 = require("./config/main");
//init express
var app = express();
//init mongoose
mongoose.connect(main_1.default.db);
//express midleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(logger('dev'));
app.use(helmet());
app.use(cors());
// router
v1_1.default(app);
app.use('/users', require('./router/users'));
//init server
var server;
if (process.env.NODE_ENV !== main_1.default.test_env) {
    server = app.listen(main_1.default.port, function () {
        console.log('server listening on port ${config.port}');
    });
}
else {
    server = app.listen(main_1.default.test_port, function () {
        console.log('server listening on port ${config.test_port}');
    });
}
exports.default = server;
//# sourceMappingURL=server.js.map