import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Posts from './components/Posts';
import Postform from './components/Postform';
import Home from './components/Home';
import Login from './components/Login';
import AuthenticatedComponent from './components/AuthenticatedComponent';
import MapContainer from './components/map/MapContainer';

import store from './store';



class App extends Component {
  render() {
    return (
        <Provider store={store}>
          <BrowserRouter>
            <Switch>
              <Route path = "/Login" exact component={Login} />
              <Route path = "/" exact component={Home} />
              <Route path = "/Posts" exact component={Posts} />
              <Route path = "/map" exact component={MapContainer} />
              <AuthenticatedComponent>
               <Route path = "/Protected" exact component={Postform} />
              </AuthenticatedComponent> 
            </Switch>
          </BrowserRouter>
        </Provider>
    );
  }
}

export default App;
