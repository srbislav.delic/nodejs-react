import { FETCH_POSTS, NEW_POST } from './types';
import { getJwt } from '../helpers/jwt';

export const fetchPosts = () => dispatch => {
        fetch('http://localhost:3001/api/posts')
        .then(res => res.json())
        .then(posts => dispatch({
            type: FETCH_POSTS,
            payload: posts
        }));
};

export const createPost = (postData) => dispatch => {
    const jwt = getJwt();
    fetch('http://localhost:3001/api/posts', {
        method: 'POST',
        headers: {
            'content-type': 'application/json',
            'Authorization': jwt
        },
        body: JSON.stringify(postData)
        })
        .then(res => res.json())
        .then(post => dispatch({
            type: NEW_POST,
            payload: post
        })
    );
};