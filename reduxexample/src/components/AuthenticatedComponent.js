import React, { Component } from 'react';
import { getJwt } from '../helpers/jwt';
import axios from 'axios';
import { withRouter } from 'react-router-dom';

class AuthenticatedComponent extends Component {
    state = {
        user: undefined
    }

    componentDidMount() {
        const jwt = getJwt();
        if(!jwt) {
            this.props.history.push('/Login');
        }
        axios.get('http://localhost:3001/users/getUser/', { headers: { Authorization: jwt }})
            .then(res => this.setState({
                user: res.data
            })).catch(err => {
                localStorage.removeItem('cool-jwt');
                this.props.history.push('/Login');
            });
    }

  render() {
    if(this.state.user===undefined) {
        return (
            <div><h1>Loading ...</h1></div>
        );
    }
    return (
      <div>
          {this.props.children}
      </div>
    )
  }
}

export default withRouter(AuthenticatedComponent);
