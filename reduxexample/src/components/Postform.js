import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createPost } from '../actions/postActions';

class Postform extends Component {
  state = {
    title: '',
    content: ''
  }

  onChange = (e) => {
    this.setState({[e.target.name]: e.target.value});
  };

  onSubmit = (e) => {
    e.preventDefault();

    const post = {
        title: this.state.title,
        content: this.state.content
    };
    
    this.props.createPost(post);
    this.setState({
      title: '',
      content: ''
    });

    this.props.history.push('/Posts');
  }

  render() {
    return (
      <div>
        <h1>Add post</h1>
        <form onSubmit={this.onSubmit}>
            <div>
                <label>Title: </label><br />
                <input type = "text" name="title" onChange={this.onChange} value={this.state.title}/>
            </div>
            <br />
            <div>
                <label>Content: </label><br />
                <textarea name="content" onChange={this.onChange} value={this.state.content}/>
            </div>
            <br />
            <button type="submit">Submit</button>
        </form>
      </div>
    )
  }
}

Postform.propTypes = {
  createPost: PropTypes.func.isRequired
};

export default connect(null, { createPost })(Postform);
