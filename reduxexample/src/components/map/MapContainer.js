import React from 'react';
import Map from './Map';
const { SearchBox } = require("react-google-maps/lib/components/places/SearchBox");

export default () => (
    <React.Fragment>
    <input style={{ height: '30px', width: '500px', margin: '20px', fontSize: '20px'}}onChange={()=> {}} /> 
    <button>Search</button>
    <Map
    isMarkerShown
    googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
    loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `900px` }} />}
        mapElement={<div style={{ height: `100%` }} />} 
    />
    
    </React.Fragment>
)