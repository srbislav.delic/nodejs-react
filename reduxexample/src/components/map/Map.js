import React, { Component } from 'react';
import './Map.css';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
import { SearchBox } from  'react-google-maps/lib/components/places/SearchBox';

class Map extends Component {
  state = {
    bounds: null,
    center: {
      lat: 41.9, lng: -87.624
    },
    markers: []
  }
  constructor(props) {
    super(props);
    this.refes = {searchBox: null}
  }
  
  onMapMounted = ref => {
    console.log(this.refes);
    this.refes.map = ref;
  }

  onBoundsChanged = () => {
    this.setState({
      bounds: this.refes.map.getBounds(),
      center: this.refes.map.getCenter(),
    })
  }

  onPlacesChanged = () => {
    const places = this.refes.searchBox.getPlaces();
    const bounds = new window.google.maps.LatLngBounds();
    console.log(places);
    const marker = places[0].geometry.location;
    this.setState({center: marker})
  }

  onSearchBoxMounted = ref => {
    this.refes.searchBox = ref;
  }
  render() {
    return (
        <GoogleMap
          ref={this.onMapMounted}
          defaultZoom={10}
          center={this.state.center}
          defaultCenter={{ lat: -34.397, lng: 150.644 }}
          onBoundsChanged={this.onBoundsChanged}>
          <SearchBox
            ref={this.onSearchBoxMounted}
            bounds={this.props.bounds}
            controlPosition={window.google.maps.ControlPosition.TOP_RIGHT}
            onPlacesChanged={this.onPlacesChanged}
          >
          <div>
            <input
              type="text"
              placeholder="Customized your placeholder"
              style={{
                boxSizing: `border-box`,
                border: `1px solid transparent`,
                width: `240px`,
                height: `32px`,
                marginTop: `27px`,
                padding: `0 12px`,
                borderRadius: `3px`,
                boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
                fontSize: `14px`,
                outline: `none`,
                textOverflow: `ellipses`,
              }}
            />
            </div>
          </SearchBox>
          {this.props.isMarkerShown && <Marker position={{ lat: -34.397, lng: 150.644 }} />}
          {this.props.isMarkerShown && <Marker position={{ lat: -34.197, lng: 150.644 }} />}
        </GoogleMap>
    )
  }
}

export default withScriptjs(withGoogleMap(Map));
